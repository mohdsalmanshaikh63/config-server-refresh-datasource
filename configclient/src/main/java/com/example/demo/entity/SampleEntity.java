package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "test")
public class SampleEntity {

	@Id
	@Column
	private int id;

	public int getId() {
		return id;
	}

	@Column
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "SampleEntity [id=" + id + ", name=" + name + "]";
	}
}
