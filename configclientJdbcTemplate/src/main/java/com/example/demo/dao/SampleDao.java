package com.example.demo.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.SampleEntity;

@Repository
public class SampleDao { // extends JpaRepository<SampleEntity, Integer> {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private final String FETCH_ALL = "SELECT id, name FROM test";
	
	public List<SampleEntity> findAll() {
		return jdbcTemplate.query(FETCH_ALL, new SampleMapper());
	}

}

class SampleMapper implements RowMapper<SampleEntity> {

	@Override
	public SampleEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
		SampleEntity sample = new SampleEntity();
		sample.setId(rs.getInt("id"));
		sample.setName(rs.getString("name"));
		return sample;
	}

}
