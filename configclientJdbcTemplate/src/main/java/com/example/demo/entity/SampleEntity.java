package com.example.demo.entity;

/*@Entity
@Table(name = "test")*/
public class SampleEntity {

	/*@Id
	@Column*/
	private int id;

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	// @Column
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "SampleEntity [id=" + id + ", name=" + name + "]";
	}
}
