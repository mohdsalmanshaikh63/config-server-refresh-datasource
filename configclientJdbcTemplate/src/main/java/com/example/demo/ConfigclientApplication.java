package com.example.demo;

import java.util.List;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableMBeanExport;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.jmx.support.RegistrationPolicy;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.SampleDao;
import com.example.demo.entity.SampleEntity;

@SpringBootApplication
@EnableMBeanExport(registration = RegistrationPolicy.IGNORE_EXISTING)
//@EnableAutoConfiguration
@RefreshScope
public class ConfigclientApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(ConfigclientApplication.class, args);
	}
}

@RefreshScope
@RestController
class MessageRestController {
	
	@Autowired
	SampleDao dao;

	@Value("${message:Hello default}")
	private String message;

	@RequestMapping("/message")
	public String getMessage() {
		return this.message;
	}
	
	@RequestMapping("/person")
	public List<SampleEntity> getAll() {
		return dao.findAll();
	}
	
}

@RefreshScope
@Configuration
/*@EnableJpaRepositories("com.example.demo.dao")
@EnableTransactionManagement
@EntityScan("com.example.demo.entity")
@ComponentScan("com.example.demo")*/
class DataSourceConfig {
	
	@Value("${jndi.datasource.name}")
	private String jndiName;
	
	//@RequestMapping("/jndi")
	public String getJndiName() {
		return this.jndiName;
	}
	
	@Bean
	@RefreshScope
	public DataSource getDataSource() throws NamingException {
		JndiDataSourceLookup jndiDataSourceLookup = new JndiDataSourceLookup();
		System.out.println("Datasource jndi is "+getJndiName());
		//return (DataSource) new JndiTemplate().lookup(getJndiName());
		return jndiDataSourceLookup.getDataSource(getJndiName());
	}
	
	@Bean
	@RefreshScope
	public JdbcTemplate getJbdcTemplate() throws NamingException {
		return new JdbcTemplate(getDataSource());
	}
	
	/*@Bean
	public LocalContainerEntityManagerFactoryBean entityManager() throws NamingException {

		LocalContainerEntityManagerFactoryBean entityManagerContainer = new LocalContainerEntityManagerFactoryBean();
		entityManagerContainer.setDataSource(getDataSource());
		entityManagerContainer.setPackagesToScan("com.example.demo.entity");

		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		entityManagerContainer.setJpaVendorAdapter(vendorAdapter);
		//HashMap<String, Object> properties = new HashMap<>();
		//properties.put("hibernate.generate-ddl", Boolean.TRUE.toString());
		//properties.put("hibernate.hbm2ddl.auto", "update");
		//properties.put("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
		//entityManagerContainer.setJpaPropertyMap(properties);

		return entityManagerContainer;

	}
	
	@Bean
	public PlatformTransactionManager transactionManager() throws NamingException {

		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManager().getObject());
		return transactionManager;
	}*/
}